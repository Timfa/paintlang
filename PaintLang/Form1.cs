﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintLang
{
    public partial class Form1 : Form
    {
        public Bitmap LogicMap;
        private float zoomLevel = 10;
        private ColorPicker colorPicker = new ColorPicker();
        private MemoryDisplayer memoryDisplayer = new MemoryDisplayer();
        private Directions cursorDirection = Directions.Right;
        private int cursorX, cursorY;
        private Output outputWindow = new Output();

        private Thread executionThread;

        private string name = "New Canvas";

        private bool executing = false;

        private Bitmap originalCursor;

        public Form1()
        {
            InitializeComponent();
            LoadSavedPositionAndSize();
            originalCursor = (Bitmap)executionCursor.Image;

            MouseWheel += new System.Windows.Forms.MouseEventHandler(OnMouseWheel);

            string[] args = Environment.GetCommandLineArgs();
            bool didLoad = false;

            if (args.Length > 1)
            {
                try
                {
                    for (int i = 0; i < args.Length; i++)
                    {
                        Console.WriteLine("args[{0}] == {1}", i, args[i]);
                    }

                    Console.WriteLine("USING: [" + (args.Length - 1) + "]" + args[args.Length - 1]);

                    string filePath = args[args.Length - 1];

                    string lastBit = Regex.Split(filePath, "\\\\").Last();
                    name = Regex.Split(lastBit, "\\.")[0];

                    Bitmap loaded = (Bitmap)Bitmap.FromFile(filePath);
                    LogicMap = new Bitmap(loaded);
                    loaded.Dispose();

                    didLoad = true;
                }
                catch(Exception e)
                {
                    MessageBox.Show("Could not open file");
                }
            }

            if (!didLoad)
            {
                LogicMap = new Bitmap(5, 5);

                for (int x = 0; x < LogicMap.Width; x++)
                {
                    for (int y = 0; y < LogicMap.Height; y++)
                    {
                        LogicMap.SetPixel(x, y, Color.White);
                    }
                }
            }

            RefreshLogicImage();

            colorPicker.Show();
            memoryDisplayer.Show();
            outputWindow.Show();

            outputWindow.SetName(name);
        }

        private void SavePositionAndSize()
        {
            Registry.SetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "MainFormX", Left);
            Registry.SetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "MainFormY", Top);

            Registry.SetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "MainFormW", Width);
            Registry.SetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "MainFormH", Height);
        }

        private void LoadSavedPositionAndSize()
        {
            try
            {
                StartPosition = FormStartPosition.Manual;
                Left = (int)Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "MainFormX", Left);
                Top = (int)Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "MainFormY", Top);

                Width = (int)Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "MainFormW", Width);
                Height = (int)Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "MainFormH", Height);
            }
            catch (Exception e)
            { } //Key probably doesn't exist yet, no real issue
        }
        
        private void SetCursorVisible(bool visible)
        {
            executionCursor.Visible = visible;
        }

        private void SetCursorGraphicPosition(int x, int y)
        {
            executionCursor.Left = (int)(x * zoomLevel) + pictureBox1.Left;
            executionCursor.Top = (int)(y * zoomLevel) + pictureBox1.Top;

            executionCursor.Width = (int)zoomLevel;
            executionCursor.Height = (int)zoomLevel;

            executionCursor.BackColor = LogicMap.GetPixel(x, y);
        }

        private void OnMouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            zoomLevel += e.Delta * 0.01f;

            if (zoomLevel < 1)
                zoomLevel = 1;

            if (zoomLevel > 100)
                zoomLevel = 100;

            RefreshLogicImage();
            SetCursorGraphicPosition(cursorX, cursorY);
        }

        public void ResizeImage(int newx, int newy)
        {
            Bitmap newBmp = new Bitmap(newx, newy);

            for (int x = 0; x < newx; x++)
            {
                for (int y = 0; y < newy; y++)
                {
                    if (x < LogicMap.Width && y < LogicMap.Height)
                        newBmp.SetPixel(x, y, LogicMap.GetPixel(x, y));
                    else
                        newBmp.SetPixel(x, y, Color.White);
                }
            }

            LogicMap = newBmp;

            RefreshLogicImage();
        }

        private void RefreshLogicImage()
        {
            Size sz = LogicMap.Size;
            Bitmap zoomed = (Bitmap)pictureBox1.Image;
            if (zoomed != null) zoomed.Dispose();
            
            zoomed = new Bitmap((int)(sz.Width * zoomLevel), (int)(sz.Height * zoomLevel));

            for (int x = 0; x < zoomed.Width; x++)
            {
                for (int y = 0; y < zoomed.Height; y++)
                {
                    zoomed.SetPixel(x, y, LogicMap.GetPixel((int)Math.Floor(x / zoomLevel), (int)Math.Floor(y / zoomLevel)));
                }
            }

            pictureBox1.Image = zoomed;
        }

        private void RefreshZoomedPixel(int xs, int ys)
        {
            Size sz = LogicMap.Size;
            Bitmap zoomed = (Bitmap)pictureBox1.Image;

            for (int x = (int)(xs * zoomLevel); x < (int)(xs * zoomLevel + zoomLevel); x++)
            {
                for (int y = (int)(ys * zoomLevel); y < (int)(ys * zoomLevel + zoomLevel); y++)
                {
                    zoomed.SetPixel(x, y, LogicMap.GetPixel((int)Math.Floor(x / zoomLevel), (int)Math.Floor(y / zoomLevel)));
                }
            }

            pictureBox1.Image = zoomed;
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (!executing)
            {
                SetCursorVisible(false);

                int xClick = e.X;// - pictureBox1.Left;
                int yClick = e.Y;// - pictureBox1.Top;

                float percX = (float)xClick / ((float)LogicMap.Width * zoomLevel);
                float percY = (float)yClick / ((float)LogicMap.Height * zoomLevel);

                xClick = ((int)Math.Floor(percX * LogicMap.Width));
                yClick = ((int)Math.Floor(percY * LogicMap.Height));

                if (xClick < 0)
                    xClick = 0;

                if (xClick > LogicMap.Width - 1)
                    xClick = LogicMap.Width - 1;

                if (yClick < 0)
                    yClick = 0;

                if (yClick > LogicMap.Height - 1)
                    yClick = LogicMap.Height - 1;

                LogicMap.SetPixel(xClick, yClick, colorPicker.SelectedColor);

                RefreshZoomedPixel(xClick, yClick);
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(executionThread != null)
                executionThread.Abort();

            outputWindow.SavePositionAndSize();
            memoryDisplayer.SavePositionAndSize();
            colorPicker.SavePositionAndSize();
            SavePositionAndSize();

            Application.Exit();
        }

        private void Form1_ResizeBegin(object sender, EventArgs e)
        {

        }

        private void Form1_ResizeEnd(object sender, EventArgs e)
        {
            SetCursorGraphicPosition(cursorX, cursorY);
        }

        private void executeQuickToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!executing)
            {
                ExecuteCanvas(0);
            }
        }

        private void debugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(!executing)
            {
                executionThread = new Thread(()=> { ExecuteCanvas(5); });
                executionThread.Start();
            }
        }

        private delegate void invokeDelegate();

        private void ExecuteCanvas(int interval = 0)
        {
            executing = true;

            if (interval == 0)
                outputWindow.DelayDraw = true;

            cursorDirection = Directions.Right;

            invokeDelegate del = () =>
            {
                abortToolStripMenuItem.Enabled = true;
                executeQuickToolStripMenuItem.Enabled = false;
                startQuickestToolStripMenuItem.Enabled = false;
                startSlowToolStripMenuItem.Enabled = false;
                startToolStripMenuItem.Enabled = false;
                startNormalToolStripMenuItem.Enabled = false;
                openToolStripMenuItem.Enabled = false;
                newToolStripMenuItem.Enabled = false;
                fileToolStripMenuItem.Enabled = false;
                canvasToolStripMenuItem.Enabled = false;
                clearToolStripMenuItem.Enabled = false;
                saveToolStripMenuItem.Enabled = false;
                resizeToolStripMenuItem.Enabled = false;

                outputWindow.Reset();
                outputWindow.ShowBrush = true;
                memoryDisplayer.Reset();

                if (interval > 0)
                {
                    SetCursorGraphicPosition(0, 0);
                    SetCursorVisible(true);
                    SetCursorRotation();
                }
            };

            Invoke(del);
            
            cursorX = 0;
            cursorY = 0;

            bool cont = true;

            while(cont)
            {
                del = () => { ExecutePixel(LogicMap.GetPixel(cursorX, cursorY)); };
                Invoke(del);

                switch(cursorDirection)
                {
                    case Directions.Right:
                        cursorX++;
                        while (cursorX < LogicMap.Width && LogicMap.GetPixel(cursorX, cursorY) == colorPicker.SpeedSkip)
                            cursorX++;
                        break;
                    case Directions.Left:
                        cursorX--;
                        while (cursorX >= 0 && LogicMap.GetPixel(cursorX, cursorY) == colorPicker.SpeedSkip)
                            cursorX--;
                        break;
                    case Directions.Up:
                        cursorY--;
                        while (cursorY >= 0 && LogicMap.GetPixel(cursorX, cursorY) == colorPicker.SpeedSkip)
                            cursorY--;
                        break;
                    case Directions.Down:
                        cursorY++;
                        while (cursorY < LogicMap.Height && LogicMap.GetPixel(cursorX, cursorY) == colorPicker.SpeedSkip)
                            cursorY++;
                        break;
                }

                if (cursorX < 0 || cursorX >= LogicMap.Width)
                    cont = false;

                if (cursorY < 0 || cursorY >= LogicMap.Height)
                    cont = false;
                
                if (interval > 0 && cont)
                    Thread.Sleep(interval);

                if (cont && interval > 0)
                {
                    del = () => { SetCursorVisible(true); SetCursorGraphicPosition(cursorX, cursorY); };
                    Invoke(del);
                }
            }

            cursorX = 0;
            cursorY = 0;

            del = () => 
            {
                SetCursorVisible(false);
                outputWindow.ShowBrush = false;

                abortToolStripMenuItem.Enabled = false;
                executeQuickToolStripMenuItem.Enabled = true;
                startQuickestToolStripMenuItem.Enabled = true;
                startSlowToolStripMenuItem.Enabled = true;
                startToolStripMenuItem.Enabled = true;
                startNormalToolStripMenuItem.Enabled = true;
                openToolStripMenuItem.Enabled = true;
                newToolStripMenuItem.Enabled = true;
                fileToolStripMenuItem.Enabled = true;
                canvasToolStripMenuItem.Enabled = true;
                clearToolStripMenuItem.Enabled = true;
                saveToolStripMenuItem.Enabled = true;
                resizeToolStripMenuItem.Enabled = true;
            };

            Invoke(del);

            if (interval == 0)
                outputWindow.DelayDraw = false;

            executing = false;
        }

        private void SetCursorRotation()
        {
            Bitmap turned = new Bitmap(originalCursor);

            switch (cursorDirection)
            {
                case Directions.Right:
                    executionCursor.Image = turned;
                    break;
                case Directions.Left:
                    turned.RotateFlip(RotateFlipType.Rotate180FlipNone);
                    executionCursor.Image = turned;
                    break;
                case Directions.Up:
                    turned.RotateFlip(RotateFlipType.Rotate270FlipNone);
                    executionCursor.Image = turned;
                    break;
                case Directions.Down:
                    turned.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    executionCursor.Image = turned;
                    break;
            }
        }

        private void ExecutePixel(Color color)
        {
            if(color == colorPicker.RedPaint)
            {
                outputWindow.PaintRed(memoryDisplayer.CurrentMemoryValue);
                return;
            }

            if (color == colorPicker.GreenPaint)
            {
                outputWindow.PaintGreen(memoryDisplayer.CurrentMemoryValue);
                return;
            }

            if (color == colorPicker.BluePaint)
            {
                outputWindow.PaintBlue(memoryDisplayer.CurrentMemoryValue);
                return;
            }

            if (color == colorPicker.MemoryUp)
            {
                memoryDisplayer.MemoryUp();
                return;
            }

            if (color == colorPicker.MemoryDown)
            {
                memoryDisplayer.MemoryDown();
                return;
            }

            if (color == colorPicker.MemoryPlus)
            {
                memoryDisplayer.MemoryPlus();
                return;
            }

            if (color == colorPicker.MemoryMinus)
            {
                memoryDisplayer.MemoryMinus();
                return;
            }

            if (color == colorPicker.BrushRight)
            {
                outputWindow.BrushRight();
                return;
            }

            if (color == colorPicker.BrushLeft)
            {
                outputWindow.BrushLeft();
                return;
            }

            if (color == colorPicker.BrushDown)
            {
                outputWindow.BrushDown();
                return;
            }

            if (color == colorPicker.BrushUp)
            {
                outputWindow.BrushUp();
                return;
            }

            if (color == colorPicker.Read)
            {
                InputByte input = new InputByte();
                input.SetStartVal(memoryDisplayer.CurrentMemoryValue);
                input.ShowDialog();

                memoryDisplayer.SetMemory(input.UserInput);
                return;
            }

            if (color == colorPicker.TurnR)
            {
                bool memoryZero = memoryDisplayer.CurrentMemoryValue == 0x00;

                Directions newDir = cursorDirection;

                if (!memoryZero)
                {
                    switch (cursorDirection)
                    {
                        case Directions.Right:
                            newDir = Directions.Down;
                            break;
                        case Directions.Left:
                            newDir = Directions.Up;
                            break;
                        case Directions.Up:
                            newDir = Directions.Right;
                            break;
                        case Directions.Down:
                            newDir = Directions.Left;
                            break;
                    }
                }

                cursorDirection = newDir;

                invokeDelegate del = () => {
                    SetCursorRotation();
                };
                Invoke(del);

                return;
            }

            if (color == colorPicker.TurnL)
            {
                bool memoryZero = memoryDisplayer.CurrentMemoryValue == 0x00;

                Directions newDir = cursorDirection;

                if (!memoryZero)
                {
                    switch (cursorDirection)
                    {
                        case Directions.Right:
                            newDir = Directions.Up;
                            break;
                        case Directions.Left:
                            newDir = Directions.Down;
                            break;
                        case Directions.Up:
                            newDir = Directions.Left;
                            break;
                        case Directions.Down:
                            newDir = Directions.Right;
                            break;
                    }
                }

                cursorDirection = newDir;

                invokeDelegate del = () => {
                    SetCursorRotation();
                };
                Invoke(del);

                return;
            }
        }

        private void resizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewSizePicker picker = new NewSizePicker();
            picker.SetOwner(this);
            picker.SetSize(LogicMap.Width, LogicMap.Height);
            picker.ShowDialog();
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!executing)
            {
                executionThread = new Thread(() => { ExecuteCanvas(5); });
                executionThread.Start();
            }
        }

        private void abortToolStripMenuItem_Click(object sender, EventArgs e)
        {
            executionThread.Abort();
            executing = false;
            
            abortToolStripMenuItem.Enabled = false;
            executeQuickToolStripMenuItem.Enabled = true;
            startQuickestToolStripMenuItem.Enabled = true;
            startSlowToolStripMenuItem.Enabled = true;
            startToolStripMenuItem.Enabled = true;
            startNormalToolStripMenuItem.Enabled = true;
            openToolStripMenuItem.Enabled = true;
            newToolStripMenuItem.Enabled = true;
            fileToolStripMenuItem.Enabled = true;
            canvasToolStripMenuItem.Enabled = true;
            clearToolStripMenuItem.Enabled = true;
            saveToolStripMenuItem.Enabled = true;
            resizeToolStripMenuItem.Enabled = true;
        }

        private void startNormalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!executing)
            {
                executionThread = new Thread(() => { ExecuteCanvas(50); });
                executionThread.Start();
            }
        }

        private void startSlowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!executing)
            {
                executionThread = new Thread(() => { ExecuteCanvas(500); });
                executionThread.Start();
            }
        }

        private void startQuickestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!executing)
            {
                executionThread = new Thread(() => { ExecuteCanvas(1); });
                executionThread.Start();
            }
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int x = 0; x < LogicMap.Width; x++)
            {
                for (int y = 0; y < LogicMap.Height; y++)
                {
                    LogicMap.SetPixel(x, y, Color.White);
                }
            }

            RefreshLogicImage();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewSizePicker picker = new NewSizePicker();
            picker.SetOwner(this);
            picker.SetSize(LogicMap.Width, LogicMap.Height);
            picker.ShowDialog();

            for (int x = 0; x < LogicMap.Width; x++)
            {
                for (int y = 0; y < LogicMap.Height; y++)
                {
                    LogicMap.SetPixel(x, y, Color.White);
                }
            }
            
            RefreshLogicImage();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.FileName = name + ".bmp";

            saveFileDialog1.Filter = "Bitmap|*.bmp";

            DialogResult result = saveFileDialog1.ShowDialog();

            if (result != DialogResult.OK)
                return;

            LogicMap.Save(saveFileDialog1.FileName);
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.FileName = "canvas.bmp";
            openFileDialog1.Filter = "Bitmap|*.bmp";

            DialogResult result = openFileDialog1.ShowDialog();

            if (result != DialogResult.OK)
                return;

            string lastBit = Regex.Split(openFileDialog1.FileName, "\\\\").Last();
            name = Regex.Split(lastBit, "\\.")[0];

            Bitmap loaded = (Bitmap)Bitmap.FromFile(openFileDialog1.FileName);
            LogicMap = new Bitmap(loaded);
            loaded.Dispose();

            RefreshLogicImage();
        }

        private enum Directions { Left, Right, Up, Down }
    }
}
