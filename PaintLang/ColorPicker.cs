﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintLang
{
    public partial class ColorPicker : Form
    {
        public Color RedPaint =     Color.FromArgb(255, 000, 000);
        public Color GreenPaint =   Color.FromArgb(000, 255, 000);
        public Color BluePaint =    Color.FromArgb(000, 000, 255);
        public Color Read =         Color.FromArgb(128, 128, 128);
        public Color MemoryUp =     Color.FromArgb(255, 255, 000);
        public Color MemoryDown =   Color.FromArgb(000, 255, 255);
        public Color MemoryPlus =   Color.FromArgb(255, 000, 255);
        public Color MemoryMinus =  Color.FromArgb(128, 128, 000);
        public Color BrushLeft =    Color.FromArgb(000, 128, 128);
        public Color BrushRight =   Color.FromArgb(128, 000, 128);
        public Color BrushUp =      Color.FromArgb(255, 128, 000);
        public Color BrushDown =    Color.FromArgb(255, 000, 128);
        public Color TurnR =        Color.FromArgb(128, 000, 255);
        public Color TurnL =        Color.FromArgb(000, 128, 255);
        public Color Eraser =       Color.FromArgb(255, 255, 255);
        public Color SpeedSkip =      Color.FromArgb(000, 000, 000);

        public Color SelectedColor = Color.FromArgb(255, 000, 000);

        public ColorPicker()
        {
            InitializeComponent();
            LoadSavedPositionAndSize();
            selectedColorDisplay.BackColor = SelectedColor;

            PaintRedBtn.BackColor = RedPaint;
            PaintGreenBtn.BackColor = GreenPaint;
            PaintBlueBtn.BackColor = BluePaint;
            ReadBtn.BackColor = Read;
            MemoryUpBtn.BackColor = MemoryUp;
            MemoryDownBtn.BackColor = MemoryDown;
            MemoryPlusBtn.BackColor = MemoryPlus;
            MemoryMinusBtn.BackColor = MemoryMinus;
            BrushLeftBtn.BackColor = BrushLeft;
            BrushRightBtn.BackColor = BrushRight;
            BrushUpBtn.BackColor = BrushUp;
            BrushDownBtn.BackColor = BrushDown;
            TurnButton.BackColor = TurnR;
            turnLeft.BackColor = TurnL;
            EraserBtn.BackColor = Eraser;
            eraserblack.BackColor = SpeedSkip;

            PaintRedBtn.Text = "";
            PaintGreenBtn.Text = "";
            PaintBlueBtn.Text = "";
            ReadBtn.Text = "";
            MemoryUpBtn.Text = "";
            MemoryDownBtn.Text = "";
            MemoryPlusBtn.Text = "";
            MemoryMinusBtn.Text = "";
            BrushLeftBtn.Text = "";
            BrushRightBtn.Text = "";
            BrushUpBtn.Text = "";
            BrushDownBtn.Text = "";
            TurnButton.Text = "";
            turnLeft.Text = "";
            EraserBtn.Text = "";
            eraserblack.Text = "";
        }

        public void SavePositionAndSize()
        {
            Registry.SetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "ColorFormX", Left);
            Registry.SetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "ColorFormY", Top);
        }

        private void LoadSavedPositionAndSize()
        {
            try
            {
                StartPosition = FormStartPosition.Manual;
                Left = (int)Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "ColorFormX", Left);
                Top = (int)Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "ColorFormY", Top);
            }
            catch(Exception e)
            { } //Key probably doesn't exist yet, no real issue
        }

        private void PaintRedBtn_Click(object sender, EventArgs e)
        {
            SelectedColor = RedPaint;
            selectedColorDisplay.BackColor = SelectedColor;
            colorExplainer.Text = "Paints the byte at the pointer into the red channel of the pixel under the brush position.";
        }

        private void PaintGreenBtn_Click(object sender, EventArgs e)
        {
            SelectedColor = GreenPaint;
            selectedColorDisplay.BackColor = SelectedColor;
            colorExplainer.Text = "Paints the byte at the pointer into the green channel of the pixel under the brush position.";
        }

        private void PaintBlueBtn_Click(object sender, EventArgs e)
        {
            SelectedColor = BluePaint;
            selectedColorDisplay.BackColor = SelectedColor;
            colorExplainer.Text = "Paints the byte at the pointer into the blue channel of the pixel under the brush position.";
        }

        private void ReadBtn_Click(object sender, EventArgs e)
        {
            SelectedColor = Read;
            selectedColorDisplay.BackColor = SelectedColor;
            colorExplainer.Text = "Gets a single byte from the user, which is inserted into the memory at the pointer.";
        }

        private void MemoryUpBtn_Click(object sender, EventArgs e)
        {
            SelectedColor = MemoryUp;
            selectedColorDisplay.BackColor = SelectedColor;
            colorExplainer.Text = "Moves the pointer one position upwards (-1).";
        }

        private void MemoryDownBtn_Click(object sender, EventArgs e)
        {
            SelectedColor = MemoryDown;
            selectedColorDisplay.BackColor = SelectedColor;
            colorExplainer.Text = "Moves the pointer one position downwards (+1).";
        }

        private void MemoryPlusBtn_Click(object sender, EventArgs e)
        {
            SelectedColor = MemoryPlus;
            selectedColorDisplay.BackColor = SelectedColor;
            colorExplainer.Text = "Increases the byte at the pointer by 1. Loops back to 0x00 when it reaches 0xFF and increases again.";
        }

        private void MemoryMinusBtn_Click(object sender, EventArgs e)
        {
            SelectedColor = MemoryMinus;
            selectedColorDisplay.BackColor = SelectedColor;
            colorExplainer.Text = "Decreases the byte at the pointer by 1. Loops back to 0xFF when it reaches 0x00 and decreases again.";
        }

        private void BrushLeftBtn_Click(object sender, EventArgs e)
        {
            SelectedColor = BrushLeft;
            selectedColorDisplay.BackColor = SelectedColor;
            colorExplainer.Text = "Moves the output brush one pixel to the left, unless it is already at X0.";
        }

        private void BrushRightBtn_Click(object sender, EventArgs e)
        {
            SelectedColor = BrushRight;
            selectedColorDisplay.BackColor = SelectedColor;
            colorExplainer.Text = "Moves the output brush one pixel to the right, expanding the canvas if needed.";
        }

        private void BrushUpBtn_Click(object sender, EventArgs e)
        {
            SelectedColor = BrushUp;
            selectedColorDisplay.BackColor = SelectedColor;
            colorExplainer.Text = "Moves the output brush one pixel upwards, unless it is already at Y0.";
        }

        private void BrushDownBtn_Click(object sender, EventArgs e)
        {
            SelectedColor = BrushDown;
            selectedColorDisplay.BackColor = SelectedColor;
            colorExplainer.Text = "Moves the output brush one pixel downwards, expanding the canvas if needed.";
        }

        private void TurnButton_Click(object sender, EventArgs e)
        {
            SelectedColor = TurnR;
            selectedColorDisplay.BackColor = SelectedColor;
            colorExplainer.Text = "Rotates the direction of the cursor to the right if the byte at the pointer is not 0x00.";
        }

        private void EraserBtn_Click(object sender, EventArgs e)
        {
            SelectedColor = Eraser;
            selectedColorDisplay.BackColor = SelectedColor;
            colorExplainer.Text = "Erases a pixel on the canvas, does nothing.";
        }

        private void ColorPicker_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
        }

        private void turnLeft_Click(object sender, EventArgs e)
        {
            SelectedColor = TurnL;
            selectedColorDisplay.BackColor = SelectedColor;
            colorExplainer.Text = "Rotates the direction of the cursor to the left if the byte at the pointer is not 0x00.";
        }

        private void eraserblack_Click(object sender, EventArgs e)
        {
            SelectedColor = SpeedSkip;
            selectedColorDisplay.BackColor = SelectedColor;
            colorExplainer.Text = "Optimizer. Makes the cursor immediately skip over, continuing in the same direction until it finds a non-black pixel. Faster than white.";
        }
    }
}
