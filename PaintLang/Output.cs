﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintLang
{
    public partial class Output : Form
    {
        private Bitmap bmp = new Bitmap(1, 1);

        private int brushX, brushY;
        private float zoomLevel = 10;

        private bool draw = true;

        public bool DelayDraw
        {
            get
            {
                return !draw;
            }

            set
            {
                draw = !value;

                RefreshOutputImage();
            }
        }

        public bool ShowBrush
        {
            get
            {
                return brushBox.Visible;
            }

            set
            {
                brushBox.Visible = value;
            }
        }

        private string oName = "";

        public void SetName(string name)
        {
            oName = name;
        }

        public Output()
        {
            InitializeComponent();
            LoadSavedPositionAndSize();
        }

        public void SavePositionAndSize()
        {
            Registry.SetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "OutputFormX", Left);
            Registry.SetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "OutputFormY", Top);

            Registry.SetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "OutputFormW", Width);
            Registry.SetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "OutputFormH", Height);
        }

        private void LoadSavedPositionAndSize()
        {
            try
            {
                StartPosition = FormStartPosition.Manual;
                Left = (int)Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "OutputFormX", Left);
                Top = (int)Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "OutputFormY", Top);

                Width = (int)Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "OutputFormW", Width);
                Height = (int)Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "OutputFormH", Height);
            }
            catch (Exception e)
            { } //Key probably doesn't exist yet, no real issue
        }

        public void Reset()
        {
            bmp = new Bitmap(1, 1);
            bmp.SetPixel(0, 0, Color.White);

            RefreshOutputImage();

            brushX = 0;
            brushY = 0;
        }

        public void PaintRed(byte value)
        {
            Color pixel = bmp.GetPixel(brushX, brushY);
            pixel = Color.FromArgb(value, pixel.G, pixel.B);
            bmp.SetPixel(brushX, brushY, pixel);

            if (draw)
            {
                brushBox.BackColor = pixel;
                RefreshZoomedPixel(brushX, brushY);
            }
        }

        public void PaintGreen(byte value)
        {
            Color pixel = bmp.GetPixel(brushX, brushY);
            pixel = Color.FromArgb(pixel.R, value, pixel.B);
            bmp.SetPixel(brushX, brushY, pixel);

            if (draw)
            {
                brushBox.BackColor = pixel;
                RefreshZoomedPixel(brushX, brushY);
            }
        }

        public void PaintBlue(byte value)
        {
            Color pixel = bmp.GetPixel(brushX, brushY);
            pixel = Color.FromArgb(pixel.R, pixel.G, value);
            bmp.SetPixel(brushX, brushY, pixel);

            if (draw)
            {
                brushBox.BackColor = pixel;
                RefreshZoomedPixel(brushX, brushY);
            }
        }

        private void SetBrushGraphicPosition(int x, int y)
        {
            brushBox.Left = (int)(x * zoomLevel) + pictureBox1.Left;
            brushBox.Top = (int)(y * zoomLevel) + pictureBox1.Top;

            brushBox.Width = (int)zoomLevel;
            brushBox.Height = (int)zoomLevel;

            brushBox.BackColor = bmp.GetPixel(x, y);
        }

        public void BrushUp()
        {
            brushY--;

            if (brushY < 0)
                brushY = 0;

            if(draw)
            {
                SetBrushGraphicPosition(brushX, brushY);
            }
        }

        public void BrushLeft()
        {
            brushX--;

            if (brushX < 0)
                brushX = 0;

            if (draw)
            {
                SetBrushGraphicPosition(brushX, brushY);
            }
        }

        public void BrushDown()
        {
            brushY++;

            if (brushY >= bmp.Height)
                ResizeBmp(bmp.Width, bmp.Height + 1);

            if (draw)
            {
                SetBrushGraphicPosition(brushX, brushY);
            }
        }

        public void BrushRight()
        {
            brushX++;

            if (brushX >= bmp.Width)
                ResizeBmp(bmp.Width + 1, bmp.Height);

            if (draw)
            {
                SetBrushGraphicPosition(brushX, brushY);
            }
        }

        private void ResizeBmp(int newx, int newy)
        {
            Bitmap newBmp = new Bitmap(newx, newy);

            for(int x = 0; x < newx; x++)
            {
                for(int y = 0; y < newy; y++)
                {
                    if (x < bmp.Width && y < bmp.Height)
                        newBmp.SetPixel(x, y, bmp.GetPixel(x, y));
                    else
                        newBmp.SetPixel(x, y, Color.White);
                }
            }

            bmp = newBmp;

            if(draw)
                RefreshOutputImage();
        }

        private void OnMouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            zoomLevel += e.Delta * 0.1f;

            if (zoomLevel < 1)
                zoomLevel = 1;

            if (zoomLevel > 100)
                zoomLevel = 100;

            RefreshOutputImage();
        }

        private void RefreshOutputImage()
        {
            Size sz = bmp.Size;
            Bitmap zoomed = (Bitmap)pictureBox1.Image;
            if (zoomed != null) zoomed.Dispose();

            zoomed = new Bitmap((int)(sz.Width * zoomLevel), (int)(sz.Height * zoomLevel));

            for (int x = 0; x < zoomed.Width; x++)
            {
                for (int y = 0; y < zoomed.Height; y++)
                {
                    zoomed.SetPixel(x, y, bmp.GetPixel((int)Math.Floor(x / zoomLevel), (int)Math.Floor(y / zoomLevel)));
                }
            }

            pictureBox1.Image = zoomed;
        }

        private void RefreshZoomedPixel(int xs, int ys)
        {
            Size sz = bmp.Size;
            Bitmap zoomed = (Bitmap)pictureBox1.Image;

            for (int x = (int)(xs * zoomLevel); x < (int)(xs * zoomLevel + zoomLevel); x++)
            {
                for (int y = (int)(ys * zoomLevel); y < (int)(ys * zoomLevel + zoomLevel); y++)
                {
                    zoomed.SetPixel(x, y, bmp.GetPixel((int)Math.Floor(x / zoomLevel), (int)Math.Floor(y / zoomLevel)));
                }
            }

            pictureBox1.Image = zoomed;
        }

        private void Output_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
        }

        private void Output_ResizeEnd(object sender, EventArgs e)
        {
            if (draw)
            {
                SetBrushGraphicPosition(brushX, brushY);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            saveFileDialog1.FileName = oName + "_Output.bmp";

            saveFileDialog1.Filter = "Bitmap|*.bmp";

            DialogResult result = saveFileDialog1.ShowDialog();

            if (result != DialogResult.OK)
                return;

            bmp.Save(saveFileDialog1.FileName);
        }
    }
}
