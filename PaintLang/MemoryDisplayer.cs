﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintLang
{
    public partial class MemoryDisplayer : Form
    {
        private int pointer = 0;
        private List<byte> memory = new List<byte>();

        public byte CurrentMemoryValue
        {
            get
            {
                return memory[pointer];
            }
        }

        public MemoryDisplayer()
        {
            InitializeComponent();
            LoadSavedPositionAndSize();
            memory.Add(0x00);

            RefreshWindows();
        }

        public void SavePositionAndSize()
        {
            Registry.SetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "MemoryFormX", Left);
            Registry.SetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "MemoryFormY", Top);

            Registry.SetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "MemoryFormH", Height);
        }

        private void LoadSavedPositionAndSize()
        {
            try
            {
                StartPosition = FormStartPosition.Manual;
                Left = (int)Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "MemoryFormX", Left);
                Top = (int)Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "MemoryFormY", Top);

                Height = (int)Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\BytePaint", "MemoryFormH", Height);
            }
            catch (Exception e)
            { } //Key probably doesn't exist yet, no real issue
        }

        private void RefreshWindows()
        {
            string pointerStr = "";
            string memoryStr = "";

            for(int i = 0; i < pointer; i++)
            {
                pointerStr += "\r\n";
            }

            pointerStr += "-->";

            for (int i = 0; i < memory.Count; i++)
            {
                memoryStr += "0x" + memory[i].ToString("X2").ToUpper() +"\r\n";
            }

            Pointerbox.Text = pointerStr;
            Memorybox.Text = memoryStr;
        }

        public void MemoryUp()
        {
            pointer--;

            if (pointer < 0)
                pointer = 0;

            RefreshWindows();
        }

        public void MemoryDown()
        {
            pointer++;

            if (pointer >= memory.Count)
                memory.Add(0x00);
            
            RefreshWindows();
        }
        
        public void MemoryPlus()
        {
            if (memory[pointer] != 0xFF)
                memory[pointer]++;
            else
                memory[pointer] = 0x00;

            RefreshWindows();
        }
        
        public void MemoryMinus()
        {
            if (memory[pointer] != 0x00)
                memory[pointer]--;
            else
                memory[pointer] = 0xFF;

            RefreshWindows();
        }

        public void Reset()
        {
            memory = new List<byte>();
            memory.Add(0x00);
            pointer = 0;

            RefreshWindows();
        }

        private void MemoryDisplayer_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
        }

        internal void SetMemory(byte userInput)
        {
            memory[pointer] = userInput;
            RefreshWindows();
        }
    }
}
