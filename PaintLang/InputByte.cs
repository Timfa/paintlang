﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintLang
{
    public partial class InputByte : Form
    {
        private Form1 main;
        public byte UserInput = 0x00;

        public InputByte()
        {
            InitializeComponent();
        }

        public void SetOwner(Form1 owner)
        {
            main = owner;
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void inputDec_ValueChanged(object sender, EventArgs e)
        {
            UserInput = (byte)inputDec.Value;
            hexBox.Text = "0x" + UserInput.ToString("X2");
        }

        internal void SetStartVal(byte currentMemoryValue)
        {
            inputDec.Value = (int)currentMemoryValue;
        }
    }
}
