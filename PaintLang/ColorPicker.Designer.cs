﻿namespace PaintLang
{
    partial class ColorPicker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ColorPicker));
            this.PaintRedBtn = new System.Windows.Forms.Button();
            this.PaintGreenBtn = new System.Windows.Forms.Button();
            this.PaintBlueBtn = new System.Windows.Forms.Button();
            this.ReadBtn = new System.Windows.Forms.Button();
            this.MemoryUpBtn = new System.Windows.Forms.Button();
            this.MemoryDownBtn = new System.Windows.Forms.Button();
            this.MemoryPlusBtn = new System.Windows.Forms.Button();
            this.MemoryMinusBtn = new System.Windows.Forms.Button();
            this.BrushLeftBtn = new System.Windows.Forms.Button();
            this.BrushRightBtn = new System.Windows.Forms.Button();
            this.BrushUpBtn = new System.Windows.Forms.Button();
            this.BrushDownBtn = new System.Windows.Forms.Button();
            this.TurnButton = new System.Windows.Forms.Button();
            this.EraserBtn = new System.Windows.Forms.Button();
            this.colorExplainer = new System.Windows.Forms.TextBox();
            this.selectedColorDisplay = new System.Windows.Forms.TextBox();
            this.turnLeft = new System.Windows.Forms.Button();
            this.eraserblack = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // PaintRedBtn
            // 
            this.PaintRedBtn.Location = new System.Drawing.Point(12, 102);
            this.PaintRedBtn.Name = "PaintRedBtn";
            this.PaintRedBtn.Size = new System.Drawing.Size(50, 50);
            this.PaintRedBtn.TabIndex = 0;
            this.PaintRedBtn.Text = "paintRedBtn";
            this.PaintRedBtn.UseVisualStyleBackColor = true;
            this.PaintRedBtn.Click += new System.EventHandler(this.PaintRedBtn_Click);
            // 
            // PaintGreenBtn
            // 
            this.PaintGreenBtn.Location = new System.Drawing.Point(68, 102);
            this.PaintGreenBtn.Name = "PaintGreenBtn";
            this.PaintGreenBtn.Size = new System.Drawing.Size(50, 50);
            this.PaintGreenBtn.TabIndex = 1;
            this.PaintGreenBtn.Text = "paintGreenBtn";
            this.PaintGreenBtn.UseVisualStyleBackColor = true;
            this.PaintGreenBtn.Click += new System.EventHandler(this.PaintGreenBtn_Click);
            // 
            // PaintBlueBtn
            // 
            this.PaintBlueBtn.Location = new System.Drawing.Point(12, 158);
            this.PaintBlueBtn.Name = "PaintBlueBtn";
            this.PaintBlueBtn.Size = new System.Drawing.Size(50, 50);
            this.PaintBlueBtn.TabIndex = 2;
            this.PaintBlueBtn.Text = "PaintBlueBtn";
            this.PaintBlueBtn.UseVisualStyleBackColor = true;
            this.PaintBlueBtn.Click += new System.EventHandler(this.PaintBlueBtn_Click);
            // 
            // ReadBtn
            // 
            this.ReadBtn.Location = new System.Drawing.Point(68, 158);
            this.ReadBtn.Name = "ReadBtn";
            this.ReadBtn.Size = new System.Drawing.Size(50, 50);
            this.ReadBtn.TabIndex = 3;
            this.ReadBtn.Text = "ReadBtn";
            this.ReadBtn.UseVisualStyleBackColor = true;
            this.ReadBtn.Click += new System.EventHandler(this.ReadBtn_Click);
            // 
            // MemoryUpBtn
            // 
            this.MemoryUpBtn.Location = new System.Drawing.Point(12, 214);
            this.MemoryUpBtn.Name = "MemoryUpBtn";
            this.MemoryUpBtn.Size = new System.Drawing.Size(50, 50);
            this.MemoryUpBtn.TabIndex = 4;
            this.MemoryUpBtn.Text = "MemoryUpBtn";
            this.MemoryUpBtn.UseVisualStyleBackColor = true;
            this.MemoryUpBtn.Click += new System.EventHandler(this.MemoryUpBtn_Click);
            // 
            // MemoryDownBtn
            // 
            this.MemoryDownBtn.Location = new System.Drawing.Point(68, 214);
            this.MemoryDownBtn.Name = "MemoryDownBtn";
            this.MemoryDownBtn.Size = new System.Drawing.Size(50, 50);
            this.MemoryDownBtn.TabIndex = 5;
            this.MemoryDownBtn.Text = "MemoryDownBtn";
            this.MemoryDownBtn.UseVisualStyleBackColor = true;
            this.MemoryDownBtn.Click += new System.EventHandler(this.MemoryDownBtn_Click);
            // 
            // MemoryPlusBtn
            // 
            this.MemoryPlusBtn.Location = new System.Drawing.Point(12, 270);
            this.MemoryPlusBtn.Name = "MemoryPlusBtn";
            this.MemoryPlusBtn.Size = new System.Drawing.Size(50, 50);
            this.MemoryPlusBtn.TabIndex = 6;
            this.MemoryPlusBtn.Text = "MemoryPlusBtn";
            this.MemoryPlusBtn.UseVisualStyleBackColor = true;
            this.MemoryPlusBtn.Click += new System.EventHandler(this.MemoryPlusBtn_Click);
            // 
            // MemoryMinusBtn
            // 
            this.MemoryMinusBtn.Location = new System.Drawing.Point(68, 270);
            this.MemoryMinusBtn.Name = "MemoryMinusBtn";
            this.MemoryMinusBtn.Size = new System.Drawing.Size(50, 50);
            this.MemoryMinusBtn.TabIndex = 7;
            this.MemoryMinusBtn.Text = "MemoryMinusBtn";
            this.MemoryMinusBtn.UseVisualStyleBackColor = true;
            this.MemoryMinusBtn.Click += new System.EventHandler(this.MemoryMinusBtn_Click);
            // 
            // BrushLeftBtn
            // 
            this.BrushLeftBtn.Location = new System.Drawing.Point(124, 102);
            this.BrushLeftBtn.Name = "BrushLeftBtn";
            this.BrushLeftBtn.Size = new System.Drawing.Size(50, 50);
            this.BrushLeftBtn.TabIndex = 8;
            this.BrushLeftBtn.Text = "BrushLeftBtn";
            this.BrushLeftBtn.UseVisualStyleBackColor = true;
            this.BrushLeftBtn.Click += new System.EventHandler(this.BrushLeftBtn_Click);
            // 
            // BrushRightBtn
            // 
            this.BrushRightBtn.Location = new System.Drawing.Point(124, 158);
            this.BrushRightBtn.Name = "BrushRightBtn";
            this.BrushRightBtn.Size = new System.Drawing.Size(50, 50);
            this.BrushRightBtn.TabIndex = 9;
            this.BrushRightBtn.Text = "BrushRightBtn";
            this.BrushRightBtn.UseVisualStyleBackColor = true;
            this.BrushRightBtn.Click += new System.EventHandler(this.BrushRightBtn_Click);
            // 
            // BrushUpBtn
            // 
            this.BrushUpBtn.Location = new System.Drawing.Point(124, 214);
            this.BrushUpBtn.Name = "BrushUpBtn";
            this.BrushUpBtn.Size = new System.Drawing.Size(50, 50);
            this.BrushUpBtn.TabIndex = 10;
            this.BrushUpBtn.Text = "BrushUpBtn";
            this.BrushUpBtn.UseVisualStyleBackColor = true;
            this.BrushUpBtn.Click += new System.EventHandler(this.BrushUpBtn_Click);
            // 
            // BrushDownBtn
            // 
            this.BrushDownBtn.Location = new System.Drawing.Point(124, 270);
            this.BrushDownBtn.Name = "BrushDownBtn";
            this.BrushDownBtn.Size = new System.Drawing.Size(50, 50);
            this.BrushDownBtn.TabIndex = 11;
            this.BrushDownBtn.Text = "BrushDownBtn";
            this.BrushDownBtn.UseVisualStyleBackColor = true;
            this.BrushDownBtn.Click += new System.EventHandler(this.BrushDownBtn_Click);
            // 
            // TurnButton
            // 
            this.TurnButton.Location = new System.Drawing.Point(12, 326);
            this.TurnButton.Name = "TurnButton";
            this.TurnButton.Size = new System.Drawing.Size(50, 50);
            this.TurnButton.TabIndex = 12;
            this.TurnButton.Text = "TurnButton";
            this.TurnButton.UseVisualStyleBackColor = true;
            this.TurnButton.Click += new System.EventHandler(this.TurnButton_Click);
            // 
            // EraserBtn
            // 
            this.EraserBtn.Location = new System.Drawing.Point(124, 326);
            this.EraserBtn.Name = "EraserBtn";
            this.EraserBtn.Size = new System.Drawing.Size(50, 50);
            this.EraserBtn.TabIndex = 13;
            this.EraserBtn.Text = "Eraser";
            this.EraserBtn.UseVisualStyleBackColor = true;
            this.EraserBtn.Click += new System.EventHandler(this.EraserBtn_Click);
            // 
            // colorExplainer
            // 
            this.colorExplainer.Location = new System.Drawing.Point(12, 25);
            this.colorExplainer.Multiline = true;
            this.colorExplainer.Name = "colorExplainer";
            this.colorExplainer.Size = new System.Drawing.Size(162, 71);
            this.colorExplainer.TabIndex = 14;
            // 
            // selectedColorDisplay
            // 
            this.selectedColorDisplay.Location = new System.Drawing.Point(12, 6);
            this.selectedColorDisplay.MaximumSize = new System.Drawing.Size(162, 20);
            this.selectedColorDisplay.Name = "selectedColorDisplay";
            this.selectedColorDisplay.ReadOnly = true;
            this.selectedColorDisplay.Size = new System.Drawing.Size(162, 20);
            this.selectedColorDisplay.TabIndex = 15;
            // 
            // turnLeft
            // 
            this.turnLeft.Location = new System.Drawing.Point(68, 326);
            this.turnLeft.Name = "turnLeft";
            this.turnLeft.Size = new System.Drawing.Size(50, 50);
            this.turnLeft.TabIndex = 16;
            this.turnLeft.Text = "turnLeft";
            this.turnLeft.UseVisualStyleBackColor = true;
            this.turnLeft.Click += new System.EventHandler(this.turnLeft_Click);
            // 
            // eraserblack
            // 
            this.eraserblack.Location = new System.Drawing.Point(12, 382);
            this.eraserblack.Name = "eraserblack";
            this.eraserblack.Size = new System.Drawing.Size(162, 50);
            this.eraserblack.TabIndex = 17;
            this.eraserblack.Text = "Eraser2";
            this.eraserblack.UseVisualStyleBackColor = true;
            this.eraserblack.Click += new System.EventHandler(this.eraserblack_Click);
            // 
            // ColorPicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(186, 442);
            this.Controls.Add(this.eraserblack);
            this.Controls.Add(this.turnLeft);
            this.Controls.Add(this.selectedColorDisplay);
            this.Controls.Add(this.colorExplainer);
            this.Controls.Add(this.EraserBtn);
            this.Controls.Add(this.TurnButton);
            this.Controls.Add(this.BrushDownBtn);
            this.Controls.Add(this.BrushUpBtn);
            this.Controls.Add(this.BrushRightBtn);
            this.Controls.Add(this.BrushLeftBtn);
            this.Controls.Add(this.MemoryMinusBtn);
            this.Controls.Add(this.MemoryPlusBtn);
            this.Controls.Add(this.MemoryDownBtn);
            this.Controls.Add(this.MemoryUpBtn);
            this.Controls.Add(this.ReadBtn);
            this.Controls.Add(this.PaintBlueBtn);
            this.Controls.Add(this.PaintGreenBtn);
            this.Controls.Add(this.PaintRedBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ColorPicker";
            this.Text = "ColorPicker";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ColorPicker_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button PaintRedBtn;
        private System.Windows.Forms.Button PaintGreenBtn;
        private System.Windows.Forms.Button PaintBlueBtn;
        private System.Windows.Forms.Button ReadBtn;
        private System.Windows.Forms.Button MemoryUpBtn;
        private System.Windows.Forms.Button MemoryDownBtn;
        private System.Windows.Forms.Button MemoryPlusBtn;
        private System.Windows.Forms.Button MemoryMinusBtn;
        private System.Windows.Forms.Button BrushLeftBtn;
        private System.Windows.Forms.Button BrushRightBtn;
        private System.Windows.Forms.Button BrushUpBtn;
        private System.Windows.Forms.Button BrushDownBtn;
        private System.Windows.Forms.Button TurnButton;
        private System.Windows.Forms.Button EraserBtn;
        private System.Windows.Forms.TextBox colorExplainer;
        private System.Windows.Forms.TextBox selectedColorDisplay;
        private System.Windows.Forms.Button turnLeft;
        private System.Windows.Forms.Button eraserblack;
    }
}