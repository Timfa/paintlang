﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintLang
{
    public partial class NewSizePicker : Form
    {
        private Form1 mainForm;

        public NewSizePicker()
        {
            InitializeComponent();
        }

        public void SetOwner(Form1 form)
        {
            mainForm = form;
        }

        public void SetSize(int x, int y)
        {
            widthBox.Value = x;
            heightBox.Value = y;
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            mainForm.ResizeImage((int)widthBox.Value, (int)heightBox.Value);
            Close();
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
