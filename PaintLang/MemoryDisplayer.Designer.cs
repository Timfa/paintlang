﻿namespace PaintLang
{
    partial class MemoryDisplayer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MemoryDisplayer));
            this.Pointerbox = new System.Windows.Forms.TextBox();
            this.Memorybox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Pointerbox
            // 
            this.Pointerbox.BackColor = System.Drawing.SystemColors.Window;
            this.Pointerbox.Dock = System.Windows.Forms.DockStyle.Left;
            this.Pointerbox.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pointerbox.Location = new System.Drawing.Point(0, 0);
            this.Pointerbox.Margin = new System.Windows.Forms.Padding(0);
            this.Pointerbox.Multiline = true;
            this.Pointerbox.Name = "Pointerbox";
            this.Pointerbox.ReadOnly = true;
            this.Pointerbox.Size = new System.Drawing.Size(33, 345);
            this.Pointerbox.TabIndex = 0;
            this.Pointerbox.Text = "-->";
            // 
            // Memorybox
            // 
            this.Memorybox.BackColor = System.Drawing.SystemColors.Window;
            this.Memorybox.Dock = System.Windows.Forms.DockStyle.Right;
            this.Memorybox.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Memorybox.Location = new System.Drawing.Point(33, 0);
            this.Memorybox.Margin = new System.Windows.Forms.Padding(0);
            this.Memorybox.Multiline = true;
            this.Memorybox.Name = "Memorybox";
            this.Memorybox.ReadOnly = true;
            this.Memorybox.Size = new System.Drawing.Size(84, 345);
            this.Memorybox.TabIndex = 1;
            // 
            // MemoryDisplayer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(117, 345);
            this.Controls.Add(this.Memorybox);
            this.Controls.Add(this.Pointerbox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(133, 900000);
            this.MinimumSize = new System.Drawing.Size(133, 39);
            this.Name = "MemoryDisplayer";
            this.Text = "MemoryDisplayer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MemoryDisplayer_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Pointerbox;
        private System.Windows.Forms.TextBox Memorybox;
    }
}