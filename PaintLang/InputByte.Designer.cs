﻿namespace PaintLang
{
    partial class InputByte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InputByte));
            this.inputDec = new System.Windows.Forms.NumericUpDown();
            this.hexBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.okBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.inputDec)).BeginInit();
            this.SuspendLayout();
            // 
            // inputDec
            // 
            this.inputDec.Location = new System.Drawing.Point(52, 12);
            this.inputDec.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.inputDec.Name = "inputDec";
            this.inputDec.Size = new System.Drawing.Size(120, 20);
            this.inputDec.TabIndex = 0;
            this.inputDec.ValueChanged += new System.EventHandler(this.inputDec_ValueChanged);
            // 
            // hexBox
            // 
            this.hexBox.Font = new System.Drawing.Font("Lucida Console", 8.25F);
            this.hexBox.Location = new System.Drawing.Point(52, 39);
            this.hexBox.Name = "hexBox";
            this.hexBox.ReadOnly = true;
            this.hexBox.Size = new System.Drawing.Size(120, 18);
            this.hexBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Input:";
            // 
            // okBtn
            // 
            this.okBtn.Location = new System.Drawing.Point(97, 65);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(75, 23);
            this.okBtn.TabIndex = 3;
            this.okBtn.Text = "OK";
            this.okBtn.UseVisualStyleBackColor = true;
            this.okBtn.Click += new System.EventHandler(this.okBtn_Click);
            // 
            // InputByte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(185, 97);
            this.Controls.Add(this.okBtn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.hexBox);
            this.Controls.Add(this.inputDec);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "InputByte";
            this.Text = "Input Byte";
            ((System.ComponentModel.ISupportInitialize)(this.inputDec)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown inputDec;
        private System.Windows.Forms.TextBox hexBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button okBtn;
    }
}